# perl-dancer2-dooraywebhook

Perl, Dancer2(web application framework) 샘플입니다. 두레이 웹훅을 처리하는 내용입니다.

## 태스크 관리

https://taiga.sys5.co/project/perl-dancer2-dooraywebhook/backlog
이 시스템에 계정 무료로 드립니다. 연락주세요.

## 소스코드

https://gitlab.com/imyaman/perl-dancer2-dooraywebhook

## 실행

```
plackup app.pl
```

## 논의

https://www.facebook.com/groups/perl.kr/
