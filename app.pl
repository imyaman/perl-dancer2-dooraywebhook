#!/usr/bin/env perl

use JSON qw(from_json decode_json to_json);
use Data::Dumper;
use Dancer2;
use common::sense;

use HTTP::CookieJar::LWP ();
use LWP::UserAgent       ();
use Data::Dumper;
use Encode;

use Fcntl;   # For O_RDWR, O_CREAT, etc.
use SDBM_File;

my ( $jar, $ua, %h );
my $key = '____:____';

$jar = HTTP::CookieJar::LWP->new;
$ua  = LWP::UserAgent->new(
  cookie_jar        => $jar,
  protocols_allowed => [ 'https' ],
);

tie(%h, 'SDBM_File', 'sdbm.bin', O_RDWR|O_CREAT, 0666)
  or die "Couldn't tie SDBM file 'filename': $!; aborting";

post '/webhook-test' => sub {
  my $self = shift;
  my ( $rbody, $evtype );
  my %ev = ("postCreated" => \&postCreated, "postSubjectChanged" => \&postSubjectChanged,
            "postMilestoneChanged" => \&postMilestoneChanged);

  headers 'content-type' => 'application/json';

  # request->body 에서 payload 를 확인합니다. request->body 는 string 입니다.
  # 이를 hash로 바꿉니다. 500 에러를 방지하기 위해 eval을 사용합니다.
  # $rbody 는 hashref 입니다.
  # eval ... or do ... 는 try/catch 와 비슷합니다.
  eval { $rbody = decode_json( request->body ); } or do {
    debug $@;
    return to_json( { "result" => "Not OK. It's not json" } );
  };

  # dooray는 웹훅을 두 가지 포맷으로 보낼 수 있습니다. Slack을 위한 포맷이면 바로 응답합니다.
  if ( $rbody->{channel} ) {
    debug "It's for Slack, not for me.";
    return to_json( { "result" => "Not OK. It's for Slack" } );
  }

  # dooray는 이벤트 타입을 알 수 없으면 바로 응답합니다.
  $evtype = $rbody->{hookEventType};
  if ( $evtype eq "" ) {
    debug "It's not for an event";
    return to_json( { "result" => "Not OK. It's not for an event" } );
  }

  return $ev{$evtype}($rbody) if (defined $ev{$evtype});

  return to_json( { "result" => "OK. Nothing to do for the event: $evtype" } );
};

sub postSubjectChanged {
  my ($body) = @_;
  debug "I got postSubjectChanged";
  return to_json( { "result" => "OK, postSubjectChanged" } );
};

sub postCreated {
  my ($body) = @_;
  my ($title);

  debug "I got postCreated";

  copyToSeaTable($body);

  $title = $body->{post}->{subject};
  if ( $title eq "" ) {
    debug "Posts must have a title";
    return to_json( { "result" => "Not OK, the post doesn't have a title" } );
  }
  debug "The task title is; $title";
  if ( $title =~ m/화상회의/ ) {
    assignToMe($body);
  }

  return to_json( { "result" => "OK. Nothing to do on postCreated" } );
}

sub postMilestoneChanged {
  my ($body) = @_;

  debug "I got postMilestoneChanged";

  updateToSeaTable($body);

  return to_json( { "result" => "OK. Nothing to do on postCreated" } );
}

sub assignToMe {
  my ($body) = @_;

  # 태스크의 담당자를 "나"로 지정합니다
  my ( $memberId, $projectId, $taskId, $payload, $uri, $req, $response );
  $memberId  = '3118892665733691672';
  $projectId = $body->{project}->{id};
  $taskId    = $body->{post}->{id};

  $payload =
'{"users":{"to":[{"type":"member", "member":{"organizationMemberId":"3118892665733691672"}}]}}';

  $uri = "https://api.dooray.com/project/v1/projects/$projectId/posts/$taskId";
  $req = HTTP::Request->new( 'PUT', $uri );
  $req->header( 'Authorization' => "dooray-api $key" );
  $req->header( 'Accept'        => 'application/json' );
  $req->content_type('application/json');
  $req->content($payload);
  debug $req;

  $response = $ua->request($req);

  if ( $response->is_success ) {
    debug $response->decoded_content;
    return to_json( { "result" => "OK, postCreated, changed the assignee" } );
  }
  else {
    debug $response->status_line;
    return to_json(
      { "result" => "OK, postCreated, but could not change the assignee" } );
  }
};

sub copyToSeaTable {
  my ($body) = @_;

  debug "copyToSeaTable";

  my $taskDetailRef;
  $taskDetailRef=getTaskDetail($body->{project}->{id}, $body->{post}->{id});
  if(! $taskDetailRef){ return to_json( { "result" => "Not OK"}); }

  my ($uri, $req, $response);
  my %payload;
  $uri = 'https://seetable.sys5.co/dtable-server/api/v1/dtables/de470eb4-cec1-4321-8f3e-53d411ff1629/rows/';
  $req = HTTP::Request->new( 'POST', $uri );
  $req->header( 'Authorization' => 'Token ____' );
  $req->header( 'Content-Type' => 'application/json' );
  $req->header( 'Accept' => 'application/json' );

  $payload{table_name}='Table1';
  $payload{row}{TaskID}=$taskDetailRef->{result}->{id};
  $payload{row}{Summary}=$taskDetailRef->{result}->{subject};
  $payload{row}{TaskNumber}=$taskDetailRef->{result}->{taskNumber};
  $payload{row}{CreatedAt}=$taskDetailRef->{result}->{createdAt};
  $payload{row}{UpdatedAt}=$taskDetailRef->{result}->{updatedAt};
  $payload{row}{Sprint}=$taskDetailRef->{result}->{milestone}->{name};
  $payload{row}{Status}=$taskDetailRef->{result}->{workflow}->{name};
  $payload{row}{Project}=$taskDetailRef->{result}->{project}->{code};
  $payload{row}{OriginalURL}= "https://demo.dooray.com/project/posts/" . $taskDetailRef->{result}->{id};

  $req->content( to_json(\%payload) );
  $response = $ua->request( $req );
  if ($response->is_success) {
    debug decode_json($response->decoded_content);
    my $responseJsonRef = decode_json($response->decoded_content);
    $h{$taskDetailRef->{result}->{id}}=$responseJsonRef->{_id};
  }else{
    debug $response;
  }
}

sub updateToSeaTable {
  my ($body) = @_;

  debug "updateToSeaTable";

  my $taskDetailRef;
  $taskDetailRef=getTaskDetail($body->{project}->{id}, $body->{post}->{id});
  if(! $taskDetailRef){ return to_json( { "result" => "Not OK"}); }

  my ($uri, $req, $response, $rowId);
  my %payload;
  $uri = 'https://seetable.sys5.co/dtable-server/api/v1/dtables/de470eb4-cec1-4321-8f3e-53d411ff1629/rows/';
  $req = HTTP::Request->new( 'PUT', $uri );
  $req->header( 'Authorization' => 'Token ____' );
  $req->header( 'Content-Type' => 'application/json' );
  $req->header( 'Accept' => 'application/json' );
  
  $rowId=$h{$taskDetailRef->{result}->{id}};
  debug "$taskDetailRef->{result}->{id} >> SDBM $rowId";
  $payload{table_name}='Table1';
  $payload{row_id}=$rowId;
  $payload{row}{Sprint}=$taskDetailRef->{result}->{milestone}->{name};

  $req->content( to_json(\%payload) );
  $response = $ua->request( $req );
  if ($response->is_success) {
    debug decode_json($response->decoded_content);
  }else{
    debug $response;
  }
}

sub getTaskDetail {
  my ($projectId, $taskId) = @_;
  my ($uri, $req, $json, $response);
  my %taskDetail;

  my ($uri, $req, $json, $response);
  $uri="https://api.dooray.com/project/v1/projects/$projectId/posts/$taskId";
  $req=HTTP::Request->new('GET', $uri);
  $req->header( 'Authorization' => "dooray-api $key" );
  $req->header( 'Accept' => 'application/json' );
  $response = $ua->request( $req );

  if ($response->is_success) {
    return from_json($response->decoded_content);
  }else{
    return undef;
  }
}

start;
